#ifndef __led__
#define __led__


void init_led_hw(void);
void led_on(unsigned char index);
void led_off(unsigned char index);

#endif