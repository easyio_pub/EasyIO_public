#ifndef __sim900__
#define __sim900__

extern void init_modem_hw(void);
extern void init_sim900_modem(void);
extern void disable_dtr(void);
extern void enable_dtr(void);
extern const char *get_apn(unsigned int code );
#endif