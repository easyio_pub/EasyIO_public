/*
 * File      : led.c
 * This file is part of RT-Thread RTOS
 * COPYRIGHT (C) 2009, RT-Thread Development Team
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rt-thread.org/license/LICENSE
 *
 * Change Logs:
 * Date           Author       Notes
 * 2009-01-05     Bernard      the first version
 */
#include <rtthread.h>
#include <stm32f10x.h>

#include "gpio.h"

void init_gpio(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_OD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Pin   = (GPIO_Pin_13|GPIO_Pin_15);
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin   = (GPIO_Pin_14);
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	GPIO_ResetBits(GPIOC, GPIO_Pin_13);
	GPIO_ResetBits(GPIOC, GPIO_Pin_14);
	GPIO_ResetBits(GPIOC, GPIO_Pin_15);
}

void set_gpio(unsigned char num)
{
	switch(num)
	{
		case 0:
			GPIO_SetBits(GPIOC,GPIO_Pin_15);
			break;
		case 1:
			GPIO_SetBits(GPIOC,GPIO_Pin_14);
			break;
		case 2:
			GPIO_SetBits(GPIOC,GPIO_Pin_13);
			break;
	}
	//
}
void reset_gpio(unsigned char num)
{
	switch(num)
	{
		case 0:
			GPIO_ResetBits(GPIOC,GPIO_Pin_15);
			break;
		case 1:
			GPIO_ResetBits(GPIOC,GPIO_Pin_14);
			break;
		case 2:
			GPIO_ResetBits(GPIOC,GPIO_Pin_13);
			break;
	}
	//
}
