//#include "sim900.h"
//#include "at_cmd.h"
//#include "modem_serial.h"
//#include "common.h"

//#include <rtthread.h>
//#include <stm32f10x.h>





#include "sim900.h"
#include "at_cmd.h"
#include "modem_serial.h"
#include "common.h"

#include <rtthread.h>
#include <stm32F10x.h>

#include "app_timer.h"

#include "my_stdc_func/debugl.h"


#define SET_PWR GPIO_SetBits(GPIOC,GPIO_Pin_0);
#define RESET_PWR GPIO_ResetBits(GPIOC,GPIO_Pin_0);

#define SET_RST GPIO_SetBits(GPIOC,GPIO_Pin_1);
#define RESET_RST GPIO_ResetBits(GPIOC,GPIO_Pin_1);

static struct rt_semaphore dtr_sem;

static void config_cts_interrupt(void);

typedef struct __CR_TABLE {
	unsigned code;
	char apn[32];
	char username[32];
	char password[32];
}CR_TABLE;


#if 0
--------------------[[Ӣ��]]------------------

  --UBIQUISYS 23401 UK
  data = nil
  data =  {
    {APN = "internet", NAME="", PWD=""}
  }


  --O2 23402 UK
  data = nil
  data =  {
    {APN = "mobile.o2.co.uk", NAME="", PWD=""},
    {APN = "payandgo.o2.co.uk", NAME="", PWD=""},
    {APN = "wap.o2.co.uk", NAME="", PWD=""}
  }
	
	23402,

  --BT One 23408 UK
  data = nil
  data =  {
    {APN = "internet.btonephone.com", NAME="", PWD=""},
    {APN = "mms.btonephone.com", NAME="", PWD=""}
  }

  --O2 23410 UK
  data = nil
  data =  {
    {APN = "mobile.o2.co.uk", NAME="", PWD=""},
    {APN = "wap.o2.co.uk", NAME="", PWD=""},
    {APN = "payandgo.o2.co.uk", NAME="payandgo", PWD="password"},
    {APN = "prepay.tesco-mobile.com", NAME="", PWD=""},
    {APN = "giffgaff.com", NAME="", PWD=""}
  }
 
  --O2 MOBILE 23411 UK
  data = nil
  data =  {
    {APN = "mobile.o2.co.uk", NAME="", PWD=""},
    {APN = "payandgo.o2.co.uk", NAME="", PWD=""},
    {APN = "wap.o2.co.uk", NAME="", PWD=""},
    {APN = "mobile.o2.co.uk", NAME="", PWD=""},
    {APN = "payandgo.o2.co.uk", NAME="", PWD=""},
    {APN = "wap.o2.co.uk", NAME="", PWD=""}
  }


  --Vodafone 23415 UK
  data = nil
  data =  {
    {APN = "internet", NAME="", PWD=""},
    {APN = "wap.vodafone.co.uk", NAME="", PWD=""},
    {APN = "pp.vodafone.co.uk", NAME="", PWD=""},
    {APN = "asdamobiles.co.uk", NAME="", PWD=""},
    {APN = "payg.talkmobile.co.uk", NAME="", PWD=""},
    {APN = "talkmobile.co.uk", NAME="", PWD=""},
    {APN = "mobile.talktalk.co.uk", NAME="", PWD=""},
    {APN = "btmobile.bt.com", NAME="", PWD=""},
    {APN = "payg.mobilebysainsburys.co.uk", NAME="", PWD=""},
    {APN = "uk.lebara.mobi", NAME="", PWD=""}
  }

  --3 23420 UK
  data = nil
  data =  {
    {APN = "three.co.uk", NAME="", PWD=""},
    {APN = "3hotspot", NAME="", PWD=""}
  }

  --Lycamobile 23426 UK
  data = nil
  data =  {
    {APN = "data.lycamobile.co.uk", NAME="", PWD=""}
  }

  --Virgin 23430 UK
  data = nil
  data =  {
    {APN = "goto.virginmobile.uk", NAME="", PWD=""},
    {APN = "everywhere", NAME="", PWD=""},
    {APN = "eezone", NAME="", PWD=""},
    {APN = "internet.btonephone.com", NAME="", PWD=""},
    {APN = "mms.btonephone.com", NAME="", PWD=""},
    {APN = "btmobile.bt.com", NAME="", PWD=""},
    {APN = "mms.bt.com", NAME="", PWD=""},
    {APN = "btmobile.bt.com", NAME="", PWD=""},
    {APN = "mms.bt.com", NAME="", PWD=""}
  }

  --Lycamobile 23431 UK
  data = nil
  data =  {
    {APN = "everywhere", NAME="", PWD=""},
    {APN = "eezone", NAME="", PWD=""}
  }

  --EE Internet 23432 UK
  data = nil
  data =  {
    {APN = "everywhere", NAME="", PWD=""},
    {APN = "eezone", NAME="", PWD=""}
  }
 
  --EE Internet 23433 UK
  data = nil
  data =  {
    {APN = "everywhere", NAME="", PWD=""},
    {APN = "eezone", NAME="", PWD=""},
    {APN = "tslpaygnet", NAME="", PWD=""},
    {APN = "tslmms", NAME="", PWD=""}
  }

  --EE Internet 23434 UK
  data = nil
  data =  {
    {APN = "everywhere", NAME="", PWD=""},
    {APN = "eezone", NAME="", PWD=""},
    {APN = "orangeinternet", NAME="", PWD=""},
    {APN = "orangemms", NAME="", PWD=""}
  }

  --Truphone 23425 UK
  data = nil
  data =  {
    {APN = "truphone.com", NAME="", PWD=""}
  }

  --Truphone 23450 UK
  data = nil
  data =  {
    {APN = "mms", NAME="", PWD=""},
    {APN = "pepper", NAME="", PWD=""}
  }

  --Guernsey 23455 UK
  data = nil
  data =  {
    {APN = "internet", NAME="", PWD=""},
    {APN = "mms", NAME="", PWD=""}
  }

  --Guernsey 23458 UK
  data = nil
  data =  {
    {APN = "3gpronto", NAME="", PWD=""},
    {APN = "mms.manxpronto.net", NAME="", PWD=""},
    {APN = "mms.prontogo.net", NAME="", PWD=""},
    {APN = "web.manxpronto.net", NAME="", PWD=""}
  }

  --Guernsey 23486 UK
  data = nil
  data =  {
    {APN = "everywhere", NAME="", PWD=""},
    {APN = "eezone", NAME="", PWD=""}
  }

#endif

const CR_TABLE crtable[] = {
	
	//CHN
	{46000,"cmnet","",""},
	{46692,"internet","",""},
	{46601,"internet","",""},
	{46697,"internet","",""},
	
	//UK
	{23401,"internet","",""},
	{23408,"internet.btonephone.com","",""},
	{23402,"mobile.o2.co.uk","",""},
	{23410,"mobile.o2.co.uk","",""},
	{23411,"mobile.o2.co.uk","",""},
	{23415,"internet","",""},
	{23420,"three.co.uk","",""},
	{23486,"everywhere","",""},
	{23458,"3gpronto","",""},
	{23455,"internet","",""},
	{23450,"pepper","",""},
	{23425,"truphone.com","",""},

};

const char *get_apn(unsigned int code )
{
	int i=0;
	for(i=0;i<(sizeof(crtable)/sizeof(CR_TABLE));i++)
	{
		if (crtable[i].code == code)
		{
			return crtable[i].apn;
		}
	}
	
	return 0;
	
}



static void init_sim900_ctrl_gpio(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
	
	
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_InitStructure.GPIO_Pin   = (GPIO_Pin_11|GPIO_Pin_12);
	GPIO_Init(GPIOA, &GPIO_InitStructure);

}


static void power_sim900(void)
{
	GPIO_ResetBits(GPIOA,GPIO_Pin_12);
	rt_thread_sleep(100*1);
	GPIO_SetBits(GPIOA,GPIO_Pin_12);

}

static void reset_sim900(void)
{
	GPIO_SetBits(GPIOA, GPIO_Pin_11);
	rt_thread_sleep( RT_TICK_PER_SECOND * 1 ); /* sleep 2 second and switch to other thread */
	GPIO_ResetBits(GPIOA, GPIO_Pin_11);
	rt_thread_sleep(RT_TICK_PER_SECOND * 1);

}
void enable_dtr(void)
{

}

void disable_dtr(void)
{
	
	
	//
}



static void powerModem(void)
{
	//
}


static void initModem2(void)
{
	unsigned char __UEXTDCONF_FLAG	= 0;
	unsigned char __USPM						= 0;
	unsigned char __USGC						= 0;
	unsigned char __USGC2						= 0;
	unsigned char __ATD							= 1;
	//AT+USGC?
	
	int i=0;
	int at_cmd_ret_code;
	unsigned char atcmd_cnt;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	DEBUGL->debug("reset_sim900....\r\n");
	reset_sim900();
	power_sim900();
	
	
	repower:
	
	
	
	
	for(i=0;i<1;i++)
	{
		at_command("AT\r\n",AT_AT,200,&at_cmd_ret_code);
		if (at_cmd_ret_code == AT_RESP_OK)
			return ;
	}
	
	
	
	for(i=0;i<2;i++)
	{
		at_command("AT\r\n",AT_AT,200,&at_cmd_ret_code);
		if (at_cmd_ret_code == AT_RESP_OK)
			return ;
	}
	
	DEBUGL->debug("power_sim900....\r\n");
	power_sim900();
	goto repower;
}


void init_modem_hw(void)
{
	
	init_sim900_ctrl_gpio();
	//
}


static int recv_cmux_buf(const char *resp , int len)
{
	return 1;
	//
}

static unsigned char setting_modem_status = 0;
static int setting_modem(void)
{
	int at_cmd_ret_code;
	switch(setting_modem_status)
	{
		case 0:
		{
			at_command("AT+FLO=0\r\n",AT_AT,200,&at_cmd_ret_code);
			if (at_cmd_ret_code == AT_RESP_OK)
			{
				setting_modem_status ++;
			}
			break;
		}
		case 1:
		{
			at_command("AT&K0\r\n",AT_AT,200,&at_cmd_ret_code);
			if (at_cmd_ret_code == AT_RESP_OK)
			{
				setting_modem_status ++;
			}
			break;
		}		
		case 2:
		{
			at_command("AT+CGMR\r\n",AT_AT,200,&at_cmd_ret_code);
			if (at_cmd_ret_code == AT_RESP_OK)
			{
				setting_modem_status ++;
			}
			break;
		}		
			
	}
}

void init_sim900_modem(void)
{
	
	int i,at_cmd_ret_code;
	int process_count = 0;
	
	reinit:
	
	initModem2();
	
	//at_command("AT+FLO=0\r\n",AT_AT,200,&at_cmd_ret_code);
	//at_command("AT&K0\r\n",AT_AT,200,&at_cmd_ret_code);
	
	
	
	
	process_count = 30;
	for(i=1;i<=process_count;i++)
	{
		at_command("AT+COPS=0,2\r\n",AT_AT,200,&at_cmd_ret_code);
		if (at_cmd_ret_code == AT_RESP_OK)
			break;
		
		if (i == process_count)
		{
			goto reinit;
		}
	}
	
	process_count = 2;
	for(i=1;i<=process_count;i++)
	{
		at_command("AT+GSN\r\n",AT_GSN,200,&at_cmd_ret_code);
		
		if (at_cmd_ret_code == AT_RESP_OK)
			break;
		
		if (i == process_count)
		{
			goto reinit;
		}
		
	}
	
	process_count = 60;
	for(i=1;i<=process_count;i++)
	{
		at_command("AT+COPS?\r\n",AT_COPS,200,&at_cmd_ret_code);
		
		if (at_cmd_ret_code == AT_RESP_OK)
			break;
		
		if (i == process_count)
		{
			goto reinit;
		}
		
	}
	
	//close_max9860();
	
}

