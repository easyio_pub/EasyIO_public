#include "led.h"
#include <stm32f10x.h>

void init_gpio_hw(void)
{
	//
}

void init_led_hw(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Pin   = (GPIO_Pin_2|GPIO_Pin_3);
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	GPIO_SetBits(GPIOC, GPIO_Pin_2);
	GPIO_SetBits(GPIOC, GPIO_Pin_3);
	
}

void led_on(unsigned char index)
{

	switch(index)
	{
		case 1:
			GPIO_ResetBits(GPIOC, GPIO_Pin_2);
			break;
		case 2:
			GPIO_ResetBits(GPIOC, GPIO_Pin_3);
			break;
	}
	//
}

void led_off(unsigned char index)
{

	switch(index)
	{
		case 1:
			GPIO_SetBits(GPIOC, GPIO_Pin_2);
			break;
		case 2:
			GPIO_SetBits(GPIOC, GPIO_Pin_3);
			break;
	}
	//
}

