#include "lwip_raw_api_tcp_template.h"
#include "XMPP/xmpp.h"
#include "lwip/tcpip.h"
#include "SHA1/sha1_password.h"
#include "gnss.h"
#include "led.h"
#include "MMA845X.h"
#include "app_timer.h"
#include "common.h"
#include "app_timer.h"
#include "my_stdc_func/my_stdc_func.h"


#define xmpp_tcp_client tcp_client_buffer[XMPP_TCP_CLIENT]
#define PRESENCE_STATUS_STRLEN 512

static unsigned int last_recv_vaild_pkg_timep = 0;;

static void __xmpp_srv_connected(void *arg)
{
	DEBUGL->info("XMPP Cli connected !\r\n");
	last_recv_vaild_pkg_timep = app_timer_data.app_timer_second;
	connected_xmpp_event();
	//
}

static void __xmpp_srv_disconnect(void *arg)
{
	DEBUGL->info("XMPP Cli disconnect !\r\n");
	disconn_xmpp_event();
	//
}

static void __xmpp_srv_recv(void *arg , unsigned char *buf , int len)
{
	recv_data_event((char*)buf,len);
	//
}


static void __xmpp_srv_routing(void *arg)
{
	
	LOCK_TCPIP_CORE();
	//如果是连接状态的时候就检查多久没连接了
	if (TCP_CONNECTED == xmpp_tcp_client.status_base)
	{
		
		
		if (xmpp_pma.xmpp_status == XMPP_BINDSUCC)
		{
			//如果正常登陆到了服务器
			if ((app_timer_data.app_timer_second - last_recv_vaild_pkg_timep) > 60*10)
			{
				xmpp_send_ping();
				//
			}
			
			if ((app_timer_data.app_timer_second - last_recv_vaild_pkg_timep) > 60*11)
			{
				MY_TCP_Client_stop(&xmpp_tcp_client);
				//
			}
			
		}else
		{
			//如果未正常登陆到服务器
			//
			
			if ((app_timer_data.app_timer_second - last_recv_vaild_pkg_timep) > 20)
			{
				MY_TCP_Client_stop(&xmpp_tcp_client);
			}
			
		}
	}
	
	UNLOCK_TCPIP_CORE();
	return ;

}

static void xmpp_disconn(void)
{
	MY_TCP_Client_pause(&xmpp_tcp_client);
}

static int xmpp_sendbuf(unsigned char *buf , int len)
{
	return MY_TCP_Client_write(&xmpp_tcp_client,buf,len);
	//
}

static void xmpp_rcvmsg(char *from , char *body)
{
	xmpp_send_msg(from,(char*)process_xmpp_msg(from,body));
}

static void xmpp_login(void)
{
	DEBUGL->info("XMPP Cli Login !\r\n");
	xmpp_set_presence("online","IDLE");
	//
}
static void recv_package(char *xml_pkg)
{
	last_recv_vaild_pkg_timep = app_timer_data.app_timer_second;
	return ;
}

#include "at_cmd.h"

#define XMPP_TCP_INIT 0

static char xmpp_tcp_status = XMPP_TCP_INIT;

void routing_xmpp_tcp(void)
{
	//
}

void init_xmpp_tcp(void)
{
	char xmpp_uname[32];
	struct XMPP_CB_FUNC func;
	init_xmpp_cli(MODEM_IMEI,MODEM_IMEI,XMPP_SERVER_HOSTNAME,XMPP_SERVER_PORT);
	
	//创建一个断线重练的TCP
	MY_TCP_Client_Init(&xmpp_tcp_client,XMPP_SERVER_HOSTNAME,XMPP_SERVER_PORT,1,1);
	
	xmpp_tcp_client.connected = __xmpp_srv_connected;
	xmpp_tcp_client.disconn = __xmpp_srv_disconnect;
	xmpp_tcp_client.recv = __xmpp_srv_recv;
	xmpp_tcp_client.routing = __xmpp_srv_routing;
	
	memset(&func,0x0,sizeof(func));
	
	func.dis_connect = xmpp_disconn;
	func.send_buffer = xmpp_sendbuf;
	func.recv_message = xmpp_rcvmsg;
	func.login = xmpp_login;
	func.recv_package = recv_package;
	
	set_xmpp_cb_func(&func);
	
	MY_TCP_Client_start(&xmpp_tcp_client);
	
}
