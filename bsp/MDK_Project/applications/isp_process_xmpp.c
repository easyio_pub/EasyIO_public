
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <rtthread.h>

#include "gnss.h"
#include "nmea/generate.h"
#include "XMPP/xmpp.h"
#include "app_ev.h"
#include "isp_http_tcp.h"
#include "my_stdc_func/my_stdc_func.h"
#include "my_stdc_func/debugl.h"
#include "gsmmux/easyio_cmux.h"
#include "at_cmd.h"

#define RESP_BUFFER_LEN 512

struct XMPP_MSG_PM {
	char msg[64];
	void (*cb)(char*,char*,char*);
	void *p;
};
static unsigned char tmp_resp_buffer[RESP_BUFFER_LEN];

static void resp_hello(char *from , char *in , char *out)
{
	snprintf(out,RESP_BUFFER_LEN,"hi");
}

//reboot 的回掉函数
static void reboot(void *p)
{
	rt_thread_sleep(RT_TICK_PER_SECOND*5);
	rt_reset_system();
}

extern rt_err_t post_default_event_wait(void * func , void * arg , void * func_resp , void *arg_resp , unsigned int timeout);
static void resp_reboot(char *from , char *in , char *out)
{
	logout_xmpp();
	post_default_event(reboot,0,0,0);
	//
}


static void resp_down(char *from , char *in , char*out)
{
	
	int p_len = get_parma_count(in);
	char *tmp;
	if (p_len == 1)
	{
		tmp = (char *)get_parma(0,in);
		//tmp 是要下载的文件名字
		snprintf(out,RESP_BUFFER_LEN,"DOWN [%s] OK",tmp);
		isp_stop_http_download();
		if (isp_start_http_download(tmp,from) == 0)
		{
			snprintf(out,RESP_BUFFER_LEN,"Restart download fw !");
			isp_restart_http_download();
		}else
		{
			snprintf(out,RESP_BUFFER_LEN,"Start download fw !");
		}
		
	}else
	{
		snprintf(out,RESP_BUFFER_LEN,"CMD ERROR");
	}
	
	//
}

static void resp_program(char *from , char *in , char*out)
{
	#ifdef ENABLE_OTA
	READ_CONFIG();
	fwconfig.update_code = 1;
	WRITE_CONFIG();
	#else
	snprintf(out,RESP_BUFFER_LEN,"No Support PROGRAM!");
	#endif
	//
}

static void isp(char *from , char *in , char*out)
{
	
	set_xmpp_sender(from);
	set_isp_step(ISP_HANDSHAKE_STM32);
	snprintf(out,RESP_BUFFER_LEN,"set isp step %d",ISP_HANDSHAKE_STM32);

	
}


static unsigned char *outbuf_ptr;
int AT_RESP(const char *resp , int len)
{

	int i=0;
	int l;

	snprintf((char*)outbuf_ptr,RESP_BUFFER_LEN,"%s",resp);
	
	l = strlen((char*)outbuf_ptr);

	for(i=0;i<l;i++)
	{
		if ((outbuf_ptr[i] == 0x0d) || (outbuf_ptr[i] == 0x0a))
		{
			outbuf_ptr[i] = 32;
			//
		}
	}
	
	return AT_RESP_OK;
	//
}

static void processat(char *from , char *in , char*out)
{
	int at_cmd_ret_code;
	outbuf_ptr = (unsigned char*)out;
	strcat(in,"\r\n");
	sprintf(out,"");
	cmux_at_command(2,in,AT_RESP,RT_TICK_PER_SECOND,&at_cmd_ret_code);
	
	//
}

static void systeminfo(char *from , char *in , char*out)
{
	
	//
}

struct XMPP_MSG_PM xmpp_msg_pm[] = {
	{"hello",resp_hello,0},
	{"reboot",resp_reboot,0},
	{"down=",resp_down,0},
	{"prog",resp_program,0},
	{"isp",isp,0},
	{"systeminfo",systeminfo,0},
	{"at",processat,0},

};

char *process_xmpp_msg(char *from , char *msg)
{
	int i=0;
	static char resp_str[RESP_BUFFER_LEN];
	DEBUGL->info("Recv XMPP message : %s / from %s \r\n",msg,from);
	for(i=0;i<((sizeof(xmpp_msg_pm))/(sizeof(struct XMPP_MSG_PM)));i++)
	{
		if (strstr(msg,xmpp_msg_pm[i].msg) == msg)
		{
			xmpp_msg_pm[i].cb(from,msg,resp_str);
			DEBUGL->debug("## resp str: [%s]",resp_str);
			return resp_str;
		}
	}
	resp_str[0] = 0x0;
	
	return resp_str;
}