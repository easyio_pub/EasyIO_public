#include "lwip_raw_api_tcp_template.h"
#include "serial2tcp.h"
#include "lwip/tcpip.h"
#include "app_timer.h"
#include "common.h"
#include "app_timer.h"
#include "my_stdc_func/my_stdc_func.h"


#define serial_tcp_client tcp_client_buffer[SERIAL_TCP_CLIENT]

#define HOSTNAME "evauto.jios.org"
#define HOSTPORT	5555



static void recv(void *arg,unsigned char *buf , int len)
{
	
	MY_TCP_Client_write(&serial_tcp_client,(unsigned char*)buf,len);
	//
}

static void disconn(void *arg)
{
	//
}

static void connerr(void *arg)
{
	//
}


static void routing(void *arg)
{
	//
}

static void connected(void *arg)
{
}

void init_serial_tcp(void)
{
	//创建一个断线会重新连接的TCP，并开启keepalive功能
	MY_TCP_Client_Init(&serial_tcp_client,HOSTNAME,HOSTPORT,1,1);
	//设置回掉函数
	INIT_TCPCLI_CB_FUNC(SERIAL_TCP_CLIENT);
	//开始一个tcp连接
	MY_TCP_Client_start(&serial_tcp_client);
	
}
