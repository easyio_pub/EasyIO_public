#ifndef __ota_http_tcp__
#define __ota_http_tcp__


#define BOOTLOADER_SIZE 1024*(8+6)
#define CONFIG_SIZE 1024*2
#define APP_SIZE 1024*200
#define DOWNLOAD_SIZE APP_SIZE


#define BASE_ADDR 		0x8000000
#define BOOTLOADER_ADDR BASE_ADDR
#define CONFIG_ADDR		BASE_ADDR + BOOTLOADER_SIZE
#define APP_ADDR			BASE_ADDR + BOOTLOADER_SIZE + CONFIG_SIZE
#define DOWN_ADDR			BASE_ADDR + BOOTLOADER_SIZE + CONFIG_SIZE + APP_SIZE



#define FW_CONFIG_NAME "_io7yjs"


struct FLASH_CONFIG_DATA {
	char name[16];
	unsigned char clen;
	unsigned int caddr;
	char update_code;
	int fw_size;
	unsigned char fw_md5sum[32];
};

extern struct FLASH_CONFIG_DATA fwconfig;


void ota_init_http_cli(void);
void ota_http_tcp_routing(void *p);
char ota_start_http_download(char *from , char *body);
void ota_restart_http_download(void);

#define WRITE_CONFIG()	\
		FLASH_ProgramStart(CONFIG_ADDR,CONFIG_SIZE);	\
		FLASH_AppendBuffer((u8*)(&fwconfig),sizeof(struct FLASH_CONFIG_DATA));	\
		FLASH_AppendEnd();	\
		FLASH_ProgramDone();

#define READ_CONFIG()	\
		memcpy(&fwconfig,(u32*)(CONFIG_ADDR),sizeof(struct FLASH_CONFIG_DATA));


#endif
