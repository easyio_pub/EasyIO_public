#include "ota_http_tcp.h"
#include "lwip_raw_api_tcp_template.h"
#include "MD5/md5sum.h"
#include "flash_opt.h"
#include "XMPP/xmpp.h"
#include "my_stdc_func/my_stdc_func.h"
#include "my_stdc_func/debugl.h"

//HTTP_TCP_CLIENT

static char download_from[32];

struct FLASH_CONFIG_DATA fwconfig;


enum{
	HTTP_STATUS_INIT,
	HTTP_STATUS_START,
	HTTP_STATUS_STARTED,
	HTTP_DOWNLOADING,
	HTTP_DOWNLOAD_FINISH,
	HTTP_ERROR,
	HTTP_FINISH,
};

enum{
	HTTP_GET_CONFIG,
	HTTP_GET_FW,
};

static md5_state_t state;
static md5_byte_t digest[16];
static char hex_output[16*2 + 1];

static int http_tcp_status = HTTP_STATUS_INIT;
static int http_get_status = HTTP_GET_FW;

static unsigned int download_size;	//当前下载的长度
static unsigned int content_length; //总长度

static char http_host[32] = {"www.easy-io.net"};
static int htp_port = 80;

static void send_httpget_pkg(char *path , char *host)
{
	char *buffer = rt_malloc(128);
	if (buffer > 0)
	{
		
		snprintf(buffer,128,"GET %s HTTP/1.1\r\nHOST: %s\r\nConnection: keep-alive\r\n\r\n",path,host);
		DEBUGL->debug("REQUEST : %s",buffer);
		MY_TCP_Client_write(&tcp_client_buffer[HTTP_TCP_CLIENT],(unsigned char*)buffer,strlen(buffer));
		rt_free(buffer);
	}else
	{
		http_tcp_status = HTTP_ERROR;
		
	}
}


int HTTP_GetContentLength(char *revbuf)
{
    char *p1 = NULL, *p2 = NULL;
    int HTTP_Body = 0;

    p1 = strstr(revbuf,"Content-Length");
    if(p1 == NULL)
        return -1;
    else
    {
        p2 = p1+strlen("Content-Length")+ 2; 
        HTTP_Body = atoi(p2);
        return HTTP_Body;
    }

}


static void recv_http_buf(unsigned char *buf , int len)
{
	//if (content_length)
	switch(http_get_status)
	{
		case HTTP_GET_FW:
		{
			
			char str[64];
			snprintf(str,sizeof(str),"download %d %d \r\n",download_size ,content_length);
			xmpp_send_msg(download_from,str);
			
			md5_append(&state, (const md5_byte_t *)buf,len);
			#ifdef ENABLE_OTA
			FLASH_AppendBuffer((u8*)buf,len);
			#endif
			//
			break;
		}
		case HTTP_GET_CONFIG:
		{
			
			
			break;
		}
	}
	
	
	//
	
	
}

static void recv(void *arg,unsigned char *buf , int len)
{
	
	switch(http_tcp_status)
	{
		case HTTP_STATUS_STARTED:
		{
			
			DEBUGL->debug("%s response : %s \r\n",buf);
			content_length = HTTP_GetContentLength((char*)buf);
			
			if (content_length > 0)
			{
				char *data_pos = (strstr((char*)buf,"\r\n\r\n"));
				if (data_pos > 0)
				{
					data_pos += 4;
					download_size = 0;
					

					http_tcp_status = HTTP_DOWNLOADING;
					
					download_size = (unsigned int)len - ((unsigned int)data_pos - (unsigned int)buf);
					recv_http_buf((u8*)data_pos,(unsigned int)len - ((unsigned int)data_pos - (unsigned int)buf));
					
					
					if (download_size == content_length)
					{	
						http_tcp_status = HTTP_DOWNLOAD_FINISH;
					}
				}
				//
			}else
			{
				http_tcp_status = HTTP_ERROR;
			}
			break;
		}
		
		case HTTP_DOWNLOADING:
			download_size += len;
			recv_http_buf((u8*)buf,len);
			
			if (download_size == content_length)
			{
				http_tcp_status = HTTP_DOWNLOAD_FINISH;
			}
			break;
		
		default:
			break;
	
	}
	
}

static void connected(void *arg)
{
	DEBUGL->debug("http server connected !!!\r\n");
	
	#if 1
	switch(http_get_status)
	{
		case HTTP_GET_FW:
			DEBUGL->debug("@@@@@@@@@@@@@@@@@@@@@@@@@ downloading fw ...\r\n");
			send_httpget_pkg("/rtthread.bin","www.easy-io.net");
			//send_httpget_pkg("/rthread.bin","www.easy-io.net");
			md5_init(&state);
			#ifdef ENABLE_OTA
			FLASH_ProgramStart(DOWN_ADDR,1024*200);
			#endif
		
			break;
		case HTTP_GET_CONFIG:
			rt_kprintf("@@@@@@@@@@@@@@@@@@@@@@@@@ downloading config ...\r\n");
			DEBUGL->debug("/fw_config","www.easy-io.net");
			break;
	}
	#endif
	
	//send_httpget_pkg("/rtthread.bin","www.easy-io.net");
}
static void disconn(void *arg)
{
	
	
	unsigned int di;
	
	
	if (http_get_status == HTTP_GET_FW)
	{
		char str[64];
		snprintf(str,sizeof(str),"FW MD5:");
		
		md5_finish(&state, digest);
		#ifdef ENABLE_OTA
		FLASH_AppendEnd();
		FLASH_ProgramDone();
		#endif
		for (di = 0; di < 16; ++di)
		{
			char md5str[4];
			snprintf(md5str,sizeof(md5str),"%02x",digest[di]);
			strcat(str,md5str);
		}
		DEBUGL->debug("%s\r\n",str);
		
		
		#ifdef ENABLE_OTA
		READ_CONFIG();
		//fwconfig.update_code = 1;
		WRITE_CONFIG();
		#endif
		
		//xmpp_send_msg(download_from,str);
		
		//
		//Debug flash md5
		md5_init(&state);
		md5_append(&state,(unsigned char*)DOWN_ADDR,1024*200);
		md5_finish(&state, digest);
		snprintf(str,sizeof(str),"FLASH MD5:");
		for (di = 0; di < 16; ++di)
		{
			char md5str[4];
			snprintf(md5str,sizeof(md5str),"%02x",digest[di]);
			strcat(str,md5str);
		}
		DEBUGL->debug("%s\r\n",str);
		
	
	}
	
	http_tcp_status = HTTP_FINISH;
	//
}
static void connerr(void *arg)
{
	http_tcp_status = HTTP_ERROR;
	
}
static void routing(void *arg)
{
}

void init_ota_http_cli(void)
{
	MY_TCP_Client_Init(&tcp_client_buffer[HTTP_TCP_CLIENT],http_host,htp_port,0,1);
	INIT_TCPCLI_CB_FUNC(HTTP_TCP_CLIENT);
	//
}

void ota_http_tcp_routing(void *p)
{
	switch(http_tcp_status)
	{
		case HTTP_STATUS_INIT:
			//NULL
			break;
		case HTTP_STATUS_START:
			MY_TCP_Client_start(&tcp_client_buffer[HTTP_TCP_CLIENT]);
			http_tcp_status = HTTP_STATUS_STARTED;
			break;
		case HTTP_STATUS_STARTED:
			break;
		case HTTP_DOWNLOADING:
			break;
		case HTTP_DOWNLOAD_FINISH:
			LOCK_TCPIP_CORE();
			TCP_RUN_TEST;
			MY_TCP_Client_stop(&tcp_client_buffer[HTTP_TCP_CLIENT]);
			TCP_RUN_TEST;
			UNLOCK_TCPIP_CORE();
			break;
		case HTTP_ERROR:
			http_tcp_status = HTTP_STATUS_START;
			break;
		case HTTP_FINISH:
			//如果当前是下载CONFIG
			if (http_get_status == HTTP_GET_CONFIG)
			{
				http_tcp_status = HTTP_STATUS_START;
				http_get_status = HTTP_GET_FW;
				//
			}else if (http_get_status == HTTP_GET_FW){
				//如果是下载完固件了，那么将停止
				LOCK_TCPIP_CORE();
				TCP_RUN_TEST;
				MY_TCP_Client_pause(&tcp_client_buffer[HTTP_TCP_CLIENT]);
				TCP_RUN_TEST;
				UNLOCK_TCPIP_CORE();
				http_tcp_status = HTTP_STATUS_INIT;
				http_get_status = HTTP_GET_FW;
			}
			break;
		
	}
	//
}

void ota_restart_http_download(void)
{
	
	MY_TCP_Client_stop(&tcp_client_buffer[HTTP_TCP_CLIENT]);
	http_tcp_status = HTTP_STATUS_START;
	//
}


char ota_start_http_download(char *from , char *body)
{
	if (http_tcp_status == HTTP_STATUS_INIT)
	{
		snprintf(download_from,sizeof(download_from),from);
		http_tcp_status = HTTP_STATUS_START;
		return 1;
	}
	return 0;
}
