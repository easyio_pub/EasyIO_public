#include "isp_http_tcp.h"
#include "lwip_raw_api_tcp_template.h"
#include "MD5/md5sum.h"
#include "flash_opt.h"
#include "XMPP/xmpp.h"
#include "my_stdc_func/my_stdc_func.h"
#include "extdrv/gpio.h"
#include <rtthread.h>
#include "my_stdc_func/debugl.h"


#define RUNNING_ISP			set_gpio(1);
#define RUNNING_FLASH		reset_gpio(1);

void reset_target_mcu(void)
{
	int i=0;
	set_gpio(0);
	rt_thread_sleep(50);
	reset_gpio(0);
	rt_thread_sleep(50);
	set_gpio(0);
	rt_thread_sleep(50);
}


enum{
	HTTP_STATUS_INIT,
	HTTP_STATUS_START,
	HTTP_STATUS_STARTED,
	HTTP_DOWNLOADING,
	HTTP_DOWNLOAD_FINISH,
	HTTP_ERROR,
	HTTP_FINISH,
};

enum{
	HTTP_GET_CONFIG,
	HTTP_GET_FW,
};

static md5_state_t state;
static md5_byte_t digest[16];
static char hex_output[16*2 + 1];

static int http_tcp_status = HTTP_STATUS_INIT;
static int http_get_status = HTTP_GET_FW;

static unsigned int download_size;	//当前下载的长度
static unsigned int content_length; //总长度

static char xmpp_sender[64];
static char http_host[64] = {0x0};
static char http_path[128] = {0x0};
static int http_port = 80;

void set_xmpp_sender(char *sender)
{
	snprintf(xmpp_sender,sizeof(xmpp_sender),"%s",sender);
}

#include "app_timer.h"
static unsigned int priv_send_tmr = 0;
static void xmpp_send_msg_delay(char *from , char *msg , unsigned int tmr)
{
	
	if ((app_timer_data.app_timer_second - priv_send_tmr) < tmr)
	{
		return;
		//
	}
	
	//
	xmpp_send_msg(from,msg);
	priv_send_tmr = app_timer_data.app_timer_second;
	//
}

static void send_httpget_pkg(char *path , char *host)
{
	char *buffer = rt_malloc(128);
	if (buffer > 0)
	{
		
		snprintf(buffer,128,"GET /%s HTTP/1.1\r\nHOST: %s\r\nConnection: keep-alive\r\n\r\n",path,host);
		DEBUGL->debug("REQUEST : %s",buffer);
		MY_TCP_Client_write(&tcp_client_buffer[HTTP_TCP_CLIENT],(unsigned char*)buffer,strlen(buffer));
		rt_free(buffer);
	}else
	{
		http_tcp_status = HTTP_ERROR;
		
	}
}


static int HTTP_GetContentLength(char *revbuf)
{
    char *p1 = NULL, *p2 = NULL;
    int HTTP_Body = 0;

    p1 = strstr(revbuf,"Content-Length");
    if(p1 == NULL)
        return -1;
    else
    {
        p2 = p1+strlen("Content-Length")+ 2; 
        HTTP_Body = atoi(p2);
        return HTTP_Body;
    }

}


static void recv_http_buf(unsigned char *buf , int len)
{
	//if (content_length)
	switch(http_get_status)
	{
		case HTTP_GET_FW:
		{
			
			char str[64];
			snprintf(str,sizeof(str),"FINISHED:%d KB TOTAL:%d KB",download_size ,content_length,(int)(((float)download_size/(float)content_length)*100));
			xmpp_send_msg_delay(xmpp_sender,str,5);
			
			md5_append(&state, (const md5_byte_t *)buf,len);
			FLASH_AppendBuffer((u8*)buf,len);
			
			//
			break;
		}
		case HTTP_GET_CONFIG:
		{
			
			
			break;
		}
	}
	
	
	//
	
	
}

static void recv(void *arg,unsigned char *buf , int len)
{
	
	switch(http_tcp_status)
	{
		case HTTP_STATUS_STARTED:
		{
			
			DEBUGL->debug("%s response : %s \r\n",buf);
			content_length = HTTP_GetContentLength((char*)buf);
			
			if (content_length > 0)
			{
				char *data_pos = (strstr((char*)buf,"\r\n\r\n"));
				if (data_pos > 0)
				{
					data_pos += 4;
					download_size = 0;
					

					http_tcp_status = HTTP_DOWNLOADING;
					
					download_size = (unsigned int)len - ((unsigned int)data_pos - (unsigned int)buf);
					recv_http_buf((u8*)data_pos,(unsigned int)len - ((unsigned int)data_pos - (unsigned int)buf));
					
					
					if (download_size == content_length)
					{	
						http_tcp_status = HTTP_DOWNLOAD_FINISH;
					}
				}
				//
			}else
			{
				http_tcp_status = HTTP_ERROR;
			}
			break;
		}
		
		case HTTP_DOWNLOADING:
			download_size += len;
			recv_http_buf((u8*)buf,len);
			
			if (download_size == content_length)
			{
				http_tcp_status = HTTP_DOWNLOAD_FINISH;
			}
			break;
		
		default:
			break;
	
	}
	
}

static void connected(void *arg)
{
	DEBUGL->debug("http server connected !!!\r\n");
	
	#if 1
	switch(http_get_status)
	{
		case HTTP_GET_FW:
			DEBUGL->debug("@@@@@@@@@@@@@@@@@@@@@@@@@ downloading fw ...\r\n");
			//send_httpget_pkg("rtthread.bin","www.easy-io.net");
			send_httpget_pkg(http_path,http_host);
			//send_httpget_pkg("/rthread.bin","www.easy-io.net");
			md5_init(&state);
			FLASH_ProgramStart(DOWN_ADDR,1024*200);
		
		
			break;
		case HTTP_GET_CONFIG:
			DEBUGL->debug("@@@@@@@@@@@@@@@@@@@@@@@@@ downloading config ...\r\n");
			//send_httpget_pkg("/fw_config","www.easy-io.net");
			send_httpget_pkg(http_path,http_host);
			break;
	}
	#endif
	
	//send_httpget_pkg("/rtthread.bin","www.easy-io.net");
}
static void disconn(void *arg)
{
	
	
	unsigned int di;
	
	
	if (http_get_status == HTTP_GET_FW)
	{
		char str[64];
		snprintf(str,sizeof(str),"FW MD5:");
		
		md5_finish(&state, digest);
		FLASH_AppendEnd();
		FLASH_ProgramDone();
		
		for (di = 0; di < 16; ++di)
		{
			char md5str[4];
			snprintf(md5str,sizeof(md5str),"%02x",digest[di]);
			strcat(str,md5str);
		}
		
		strcat(str,"download finish!");
		xmpp_send_msg_delay(xmpp_sender,str,0);
	
	}
	
	http_tcp_status = HTTP_FINISH;
	//
}
static void connerr(void *arg)
{
	http_tcp_status = HTTP_ERROR;
	
}
static void routing(void *arg)
{
}



//////////////////////////

static rt_device_t isp_serial_fd = RT_NULL ;
static struct rt_semaphore isp_serial_rx_sem;

static rt_err_t usart_rx_ind(rt_device_t dev, rt_size_t size)
{
	rt_sem_release(&isp_serial_rx_sem);//当有数据的时候将锁打开
	return RT_EOK;
}

rt_err_t open_isp_serial_port(char *portname)
{
	isp_serial_fd = rt_device_find("uart2");
	if (isp_serial_fd != RT_NULL && rt_device_open(isp_serial_fd, RT_DEVICE_OFLAG_RDWR) == RT_EOK)
	{
		rt_sem_init(&isp_serial_rx_sem, "isp_sem", 0, 0);
		rt_device_set_rx_indicate(isp_serial_fd, usart_rx_ind);
		//
	}else
	{
		return -RT_ERROR;
	}
	
	return -RT_EOK;
	//
}


//阻塞读串口数据
static int read_isp_serial_data(unsigned char *buf , int len , rt_int32_t time)
{
	//只等待1秒吧
	rt_sem_take(&isp_serial_rx_sem, time) ;//RT_ETIMEOUT
	return rt_device_read(isp_serial_fd, 0, buf, len);
}

static unsigned char clearbuffer(void)
{
	static unsigned char buffer[50];
	read_isp_serial_data(buffer,50,1); //wait 10ms
	return 0;
	
	//
}

static unsigned char write_isp_buffer_ack(unsigned char *buffer , int len)
{
	clearbuffer();
	rt_device_write(isp_serial_fd,0,buffer,len);
	buffer[0] = 0x0;
	read_isp_serial_data(buffer,100,100);
	return buffer[0];
	
	//
}

static void write_isp_buffer(unsigned char *buffer , int len)
{

	rt_device_write(isp_serial_fd,0,buffer,len);
	
	//
}



static ISP_STATUS_STEP isp_step = ISP_NULL; 

void set_isp_step(ISP_STATUS_STEP step)
{
	isp_step = step;
}
ISP_STATUS_STEP get_isp_step(void)
{
	return isp_step;
}

static void isp_pargarm_entry(void *p)
{
	
	#define ISP_BUFFER_LEN 32
	#define ACK_OK 0x79
	#define ACK_NO 0x1F
	
	int error_code = 0;
	//unsigned char step = 0;
	unsigned char *buffer;
	static unsigned char __buffer[ISP_BUFFER_LEN];
	buffer = __buffer;//rt_malloc(ISP_BUFFER_LEN);
	
	if (buffer <= 0)
	{
		error_code = -1;
		goto program_err;
	}
	
	
	
	for(;;)
	{
		
		//RESET MCU TO ISP MOD
		switch(isp_step)
		{
			
			case ISP_NULL:
			{
				
				//判断输出类型是不是字符串
				//判断真的是不是字符串
				//
				//readserial_data;
				//xmpp_send_msg_delay
				
				//is_string();
				//isp_step = ISP_HANDSHAKE_STM32;
				break;
			}
			//握手
			case ISP_HANDSHAKE_STM32:
			{
				
				
				
				//连接STM32
				DEBUGL->debug("handshake!\r\n");
				
				DEBUGL->debug("reset target mcu!\r\n");
				RUNNING_ISP;
				reset_target_mcu();
				DEBUGL->debug("reset finish\r\n");
				
				memset(buffer,0x0,ISP_BUFFER_LEN);
				buffer[0] = 0x7f;
				if (write_isp_buffer_ack(buffer,1) == ACK_OK)
				{
					char str[64];
					snprintf(str,sizeof(str),"Handshake success!");
					xmpp_send_msg(xmpp_sender,str);
					isp_step = ISP_ERASE_FLASH_FLAG;
				}else
				{
					char str[64];
					snprintf(str,sizeof(str),"Handshake falid!");
					xmpp_send_msg(xmpp_sender,str);
					goto program_err;
				}
				break;
			}
			//擦除
			case ISP_ERASE_FLASH_FLAG:
			{
				//擦除FLASH
				DEBUGL->debug("erase flash\r\n");
				memset(buffer,0x0,ISP_BUFFER_LEN);
				buffer[0] = 0x43;
				buffer[1] = 0xbc;
				if (write_isp_buffer_ack(buffer,2) == ACK_OK)
				{
					isp_step = ISP_ERASE_FLAHH_ALL;
				}else
				{
					goto program_err;
				}
				
				break;
			}
			//全部擦除
			case ISP_ERASE_FLAHH_ALL:
			{
				//全部擦除
				DEBUGL->debug("erase flash all\r\n");
				memset(buffer,0x0,ISP_BUFFER_LEN);
				buffer[0] = 0xff;
				buffer[1] = 0x00;
				if (write_isp_buffer_ack(buffer,2) == ACK_OK)
				{
					isp_step = ISP_PROGRAM;
				}else
				{
					goto program_err;
				}
				break;
			}
			//编程
			case ISP_PROGRAM:
			{
				
				#define FLASH_BASEADDR 0x08000000
				unsigned int FileLen = 1024*256; //256k
				unsigned int sendnum = 0;
				unsigned int Address = 0x0;
				unsigned char *Adr;
				unsigned char AdrXOR;
				unsigned char datnum = 0;
				unsigned char Dat;
				unsigned char DatXOR;
				unsigned char i,j,k;
				unsigned char *ProgramPtr = (unsigned char*)/*0x08000000;*/DOWN_ADDR;
				unsigned int pread = 0;
				unsigned char error_cnt = 0;
				
				
				char str[64];
				snprintf(str,sizeof(str),"CONNECTED ! START PROGRAM .");
				xmpp_send_msg(xmpp_sender,str);
				

				for(;;)
				{
					
					//如果错误的太多了
					if (error_cnt >= 10)
						isp_step = ISP_PROGRAM_ERROR;
					
					memset(buffer,0x0,ISP_BUFFER_LEN);
					buffer[0] = 0x31;
					buffer[1] = 0xce;
					if (write_isp_buffer_ack(buffer,2) == ACK_OK)
					{
					}else
					{
						error_cnt++;
						DEBUGL->debug("0x31ce error\r\n");
						continue;
						//失败
						//goto program_err;
					}
					
					
					if(sendnum == FileLen)
					{
						isp_step = ISP_PROGRAM_DONE;
						break;
					}
				
					//计算当前要写入的地址
					Address = FLASH_BASEADDR + sendnum;
					Adr = (unsigned char *)&Address;
				
					AdrXOR = Adr[0];
//					for(k = 1; k < 4; k ++)
//					{
//						Address >>= 8;
//						Adr[k]=(u8)Address;
//						AdrXOR ^= Adr[k];
//					}
//					
					AdrXOR = Adr[3];
					AdrXOR ^= Adr[2];
					AdrXOR ^= Adr[1];
					AdrXOR ^= Adr[0];
					
					//发送地址和校验位
					memset(buffer,0x0,ISP_BUFFER_LEN);
					buffer[0] = Adr[3];
					buffer[1] = Adr[2];
					buffer[2] = Adr[1];
					buffer[3] = Adr[0];
					buffer[4] = AdrXOR;
					//地址发送成功
					if (write_isp_buffer_ack(buffer,5) == ACK_OK)
					{
					}else
					{
						
						DEBUGL->debug("write address error\r\n");
						error_cnt++;
						continue;
					}
					
					//开始进入下载流程
					#define WRITE_LEN 64*4 - 8
					if((FileLen-sendnum) >= WRITE_LEN)//????250???
					{
						datnum = WRITE_LEN-1;						
					}
					else
					{
						datnum = (unsigned char)(FileLen - sendnum - 1);												
					}
					
					//发送长度
					memset(buffer,0x0,ISP_BUFFER_LEN);
					buffer[0] = datnum;
					write_isp_buffer(buffer,1);
					
					
					DatXOR = datnum;
					
					
					for (j= 0; j <= datnum; j++)
					{
						Dat = ProgramPtr[pread];
						
						memset(buffer,0x0,ISP_BUFFER_LEN);
						buffer[0] = Dat;
						write_isp_buffer(buffer,1);
						
						sendnum ++;
						pread ++;
						
						DatXOR ^= Dat;

					}
					//发送校验
					memset(buffer,0x0,ISP_BUFFER_LEN);
					buffer[0] = DatXOR;
					if (write_isp_buffer_ack(buffer,1) == ACK_OK)
					{
						
						char str[64];
						snprintf(str,sizeof(str),"Program : %d/%d ",sendnum,FileLen);
						xmpp_send_msg_delay(xmpp_sender,str,5);
						
						DEBUGL->debug("write dat success %d\r\n",sendnum);
						
						//rt_thread_sleep(50);
						
					}else
					{
						//失败
						sendnum = sendnum - datnum - 1;// ????
						pread = pread - datnum - 1;
						
						DEBUGL->debug("write dat error\r\n");
						error_cnt++;
						continue;
					}
						
				}
				
				break;
			}
			//完成
			
			case ISP_PROGRAM_ERROR:
			{
				char str[64];
				snprintf(str,sizeof(str),"Program ERR.");
				xmpp_send_msg(xmpp_sender,str);
				DEBUGL->debug("ProgramERR !!!\r\n");
				goto program_err;
				break;
			}
			case ISP_PROGRAM_DONE:
			{
				//
				//程序下载完毕
				char str[64];
				snprintf(str,sizeof(str),"Program SUCCESS , reset target mcu .");
				xmpp_send_msg(xmpp_sender,str);
				
				DEBUGL->debug("ProgramDone !!!\r\n");
				RUNNING_FLASH;
				reset_target_mcu();
				goto program_done;
				break;
			
			}
			default:
				break;
			
			
		}
		
		continue;
		
		//错误处理
		program_done:
		program_err:
		isp_step = ISP_NULL;
		
	}
	
	
	return;
}

#include <rtthread.h>
static struct rt_thread isp_thread;
static rt_uint8_t isp_stack[ 1024 ];
void create_isp_thread(void)
{
	rt_err_t result;
	//初始化PPP服务
	result = rt_thread_init(&isp_thread,
													"mloop",
													isp_pargarm_entry,
													RT_NULL,
													(rt_uint8_t*)&isp_stack[0],
													sizeof(isp_stack),
													20,
													5);
	if (result == RT_EOK)
	{
			rt_thread_startup(&isp_thread);
	}
}

void init_isp_serial_port(char *name)
{
	open_isp_serial_port("uart2");
	//isp_pargarm_entry(0);
	
	
	//
}

void init_isp_http_cli(void)
{
	MY_TCP_Client_Init(&tcp_client_buffer[HTTP_TCP_CLIENT],http_host,http_port,0,1);
	INIT_TCPCLI_CB_FUNC(HTTP_TCP_CLIENT);
	//
}

void isp_http_tcp_routing(void *p)
{
	switch(http_tcp_status)
	{
		case HTTP_STATUS_INIT:
			
			//NULL
			break;
		case HTTP_STATUS_START:
			MY_TCP_Client_start(&tcp_client_buffer[HTTP_TCP_CLIENT]);
			http_tcp_status = HTTP_STATUS_STARTED;
			break;
		case HTTP_STATUS_STARTED:
			break;
		case HTTP_DOWNLOADING:
			break;
		case HTTP_DOWNLOAD_FINISH:
			LOCK_TCPIP_CORE();
			TCP_RUN_TEST;
			MY_TCP_Client_stop(&tcp_client_buffer[HTTP_TCP_CLIENT]);
			TCP_RUN_TEST;
			UNLOCK_TCPIP_CORE();
			break;
		case HTTP_ERROR:
			http_tcp_status = HTTP_STATUS_START;
			break;
		case HTTP_FINISH:
			//如果当前是下载CONFIG
			if (http_get_status == HTTP_GET_CONFIG)
			{
				http_tcp_status = HTTP_STATUS_START;
				http_get_status = HTTP_GET_FW;
				//
			}else if (http_get_status == HTTP_GET_FW){
				//如果是下载完固件了，那么将停止
				LOCK_TCPIP_CORE();
				TCP_RUN_TEST;
				MY_TCP_Client_pause(&tcp_client_buffer[HTTP_TCP_CLIENT]);
				TCP_RUN_TEST;
				UNLOCK_TCPIP_CORE();
				http_tcp_status = HTTP_STATUS_INIT;
				http_get_status = HTTP_GET_FW;
			}
			break;
		
	}
	//
}

void isp_restart_http_download(void)
{
	
	MY_TCP_Client_stop(&tcp_client_buffer[HTTP_TCP_CLIENT]);
	http_tcp_status = HTTP_STATUS_START;
	//
}

void isp_stop_http_download(void)
{
	
	MY_TCP_Client_stop(&tcp_client_buffer[HTTP_TCP_CLIENT]);
	http_tcp_status = HTTP_STATUS_INIT;
	//
}


char isp_start_http_download(char *web , char *sender)
{
	
	//sprintf(web,"http://www.easy-io.net/rtthread.bin");
	if (http_tcp_status == HTTP_STATUS_INIT)
	{
		if(strlen(web) > sizeof(http_path))
			return 0;
		
		snprintf(xmpp_sender,sizeof(xmpp_sender),sender);
		
		GetHost(web,http_host,http_path,&http_port);
		init_isp_http_cli();
		//snprintf(download_from,sizeof(download_from),web);
		http_tcp_status = HTTP_STATUS_START;
		return 1;
	}
	return 0;
}
