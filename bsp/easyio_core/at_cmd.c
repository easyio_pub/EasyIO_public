#include "at_cmd.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "my_stdc_func/debugl.h"
#include "my_stdc_func/my_stdc_func.h"
int AT_AT(const char *resp , int len)
{
	if (strstr(resp,"OK"))
		return AT_RESP_OK;
	else
		return AT_RESP_ERROR;
	//
}

int AT_CREG(const char *resp , int len)
{
	//REG: 0,1
	if (strstr(resp,"REG: 0,1"))
	{
		return AT_RESP_OK;
	}
	else 
	if (strstr(resp,"REG: 0,5"))
	{
		return AT_RESP_OK;
	}
	else
		return AT_RESP_ERROR;
}

//AT+CGDCONT
int AT_CGDCONT(const char *resp , int len)
{
	if (strstr(resp,"OK"))
		return AT_RESP_OK;
	else
		return AT_RESP_ERROR;
	//
}

int AT_ATD(const char *resp , int len)
{
	if (strstr(resp,"CONNECT"))
		return AT_RESP_CONNECT;
	else
		return AT_RESP_ERROR;
	//
}

int AT_CSQ(const char *resp , int len)
{
	if (strstr(resp,"OK"))
		return AT_RESP_CONNECT;
	else
		return AT_RESP_ERROR;
	//
}

char modem_imei[MODEM_IMEI_LEN] = {0x0};
int AT_GSN(const char *resp , int len)
{
	if (strstr(resp,"ERROR"))
	{
		modem_imei[0] = 0x0;
		return AT_RESP_ERROR;
	}
	
	else if (strstr(resp,"OK"))
	{
		
		char *_imei = strstr(resp,"OK");
		_imei -= (15+4);
		_imei[15] = 0x0;
		snprintf(modem_imei,sizeof(modem_imei),"%s",_imei);
		//DEBUGL->debug("_imei : [%s]\r\n",_imei);
		return AT_RESP_OK;
	}
	
	return AT_RESP_ERROR;
}

unsigned int network_code = 0;
int AT_COPS(const char *resp , int len)
{
	unsigned int mycode;
	char code_str[32];
	
	if (strstr(resp,"OK"))
	{
		if (get_str_fragment(resp,"\"","\"",code_str,28) == 0)
		{
			sscanf(code_str,"%d",&network_code);
			DEBUGL->debug("NET CODE: %d \r\n",network_code);
			return AT_RESP_OK;
		}
	}
	return AT_RESP_ERROR;
	//
}


int AT_PLUSPLUSPLUS(const char *resp , int len)
{
	return AT_RESP_OK;
	//
}

int AT_INTERRUPT(const char *input_str)
{
	
	if (strstr(input_str,"RING"))
	{
		//
	}
	
	return AT_RESP_OK;
	//
}
