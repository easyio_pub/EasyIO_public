#ifndef __at_cmd__
#define __at_cmd__

enum {
	AT_RESP_EMPTY = -1,
	AT_RESP_ERROR = 0,
	AT_RESP_OK,
	AT_RESP_CREG_0_1,
	AT_RESP_CREG_0_5,
	AT_RESP_CONNECT,
};

#define MODEM_IMEI_LEN 16
extern char modem_imei[MODEM_IMEI_LEN];
extern unsigned int network_code;

int AT_AT(const char *resp , int len);
int AT_CGDCONT(const char *resp , int len);
int AT_ATD(const char *resp , int len);
int AT_PLUSPLUSPLUS(const char *resp , int len);
int AT_CSQ(const char *resp , int len);
int AT_GSN(const char *resp , int len);
int AT_CREG(const char *resp , int len);
int AT_COPS(const char *resp , int len);

int AT_INTERRUPT(const char *input_str);

#endif