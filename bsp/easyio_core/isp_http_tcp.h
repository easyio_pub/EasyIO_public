#ifndef __isp_http_tcp__
#define __isp_http_tcp__

#include "ota_http_tcp.h"

typedef enum {
	ISP_NULL,
	ISP_HANDSHAKE_STM32,
	ISP_ERASE_FLASH_FLAG,
	ISP_ERASE_FLAHH_ALL,
	ISP_PROGRAM,
	ISP_PROGRAM_ERROR,
	ISP_PROGRAM_DONE,
}ISP_STATUS_STEP;
void set_isp_step(ISP_STATUS_STEP step);
ISP_STATUS_STEP get_isp_step(void);

void set_xmpp_sender(char *sender);

void init_isp_serial_port(char *port);
void init_isp_http_cli(void);
void create_isp_thread(void);
void isp_http_tcp_routing(void *p);
char isp_start_http_download(char *web , char *sender);
void isp_stop_http_download(void);
void isp_restart_http_download(void);



#endif
