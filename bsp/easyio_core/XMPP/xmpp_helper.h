#ifndef __xmpp_helper__
#define __xmpp_helper__

int get_auth_str(char * username , char * password , char * out);
char *get_xml_att(char *head , char *str_in , char * str_out , char out_lim);
int get_ping_rs_str(char *dst , char *jid , char *pingid ,  char *out , int len);
#endif
