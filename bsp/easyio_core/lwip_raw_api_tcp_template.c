#include "lwip_raw_api_tcp_template.h"
#include <stdlib.h>
#include <stdio.h>
#include "my_stdc_func/debugl.h"
//#include "my_stdc_func/debugl.h"

static err_t
tcp_client_recv_cb(void *arg, struct tcp_pcb *pcb, struct pbuf *p, err_t err)
{
	unsigned char *recv_buf;
	struct TcpClientCon *cli = (struct TcpClientCon *)arg;
	DEBUGL->debug("tcp client recv errcode %d \r\n",err);
  if(err == ERR_OK && p != NULL) {
    /* Inform TCP that we have taken the data. */
    tcp_recved(pcb, p->tot_len);
		
		//DEBUGL->debug("recv data len %d \r\n",p->tot_len);
		recv_buf = rt_malloc(p->tot_len+1);
		if (recv_buf > 0)
		{
			recv_buf[p->tot_len] = 0x0;
			pbuf_copy_partial(p,recv_buf,p->tot_len,0);
			if (cli->recv > 0)
			{
				cli->recv(cli->arg,recv_buf,p->tot_len);
			}
			rt_free(recv_buf);
		}
		//tcp_write(pcb,TCP_TestData,sizeof(TCP_TestData),0);   
  }
  else if(err == ERR_OK && p == NULL) {
		TCP_RUN_TEST;
    //MY_TCP_Client_stop(cli);
		tcp_close(cli->pcb);
		//tcp_abort(cli->pcb);
		cli->status_base = TCP_DISCONNECT;
		if (cli->disconn > 0)
			cli->disconn(cli->arg);
		TCP_RUN_TEST;
  }
	if (p > 0)
		pbuf_free(p);
  return ERR_OK;
}


static err_t tcp_sent_cb(void *arg, struct tcp_pcb *tpcb,
                              u16_t len)
{
	struct TcpClientCon *cli = (struct TcpClientCon *)arg;
	return 0;
	//
}

//连接成功
static err_t TcpCli_Connected(void *arg,struct tcp_pcb *pcb,err_t err) 
{
	
	struct TcpClientCon *cli = (struct TcpClientCon *)arg;
  tcp_recv(pcb,tcp_client_recv_cb);
	tcp_sent(pcb, tcp_sent_cb);
  cli->status_base=TCP_CONNECTED;
	if (cli->connected > 0)
		cli->connected(cli->arg);
  return ERR_OK;
} 

//连接失败
static void ClientConError(void *arg, err_t err)
{
	struct TcpClientCon *cli = (struct TcpClientCon *)arg;
	DEBUGL->debug("connect error %d \r\n",err);
	cli->status_base=TCP_CONNECT_ERR;
	if (cli->connerr > 0)
		cli->connerr(cli->arg);
}



static
void my_found(const char *name, ip_addr_t *ipaddr, void *arg)
{
	ip_addr_t result;
	unsigned char  ip[4];
	
	struct TcpClientCon *cli = (struct TcpClientCon *)arg;
	
	if (ipaddr <= 0)
	{
		DEBUGL->debug("dns error !\r\n");
		cli->status_base = TCP_DNS_ERR;
		if (cli->connerr > 0)
			cli->connerr(cli->arg);
		return ;
	}
	
	result = *ipaddr;
	
	
	ip[0] = result.addr>>24; 
	ip[1] = result.addr>>16;
	ip[2] = result.addr>>8; 
	ip[3] = result.addr; 
	
	memcpy(&cli->ipaddr,ipaddr,sizeof(cli->ipaddr));

	DEBUGL->debug("DNS SUCCESS :%d.%d.%d.%d \r\n",ip[3], ip[2], ip[1], ip[0]); 
	
	cli->status_base = TCP_DNS_FINISH;
	
	
}


struct TcpClientCon tcp_client_buffer[TCP_CLI_COUNT];
void init_tcp_client_buf(void)
{
	memset(&tcp_client_buffer,0x0,sizeof(tcp_client_buffer));
	//
}
void rout_tcp_client_buf(void)
{
	int i=0;
	for(i=0;i<TCP_CLI_COUNT;i++)
	{
		if (tcp_client_buffer[i].used == 1)
		{
			MY_TCP_Client_Routing(&tcp_client_buffer[i]);
			DEBUGL->debug("TCP INDEX [%d] STATUS [%d]\r\n",i,tcp_client_buffer[i].status_base);
		}
	}
}

void disconn_tcp_client_buf(void)
{
	int i=0;
	for(i=0;i<TCP_CLI_COUNT;i++)
	{
		if (tcp_client_buffer[i].used == 1)
		{
			TCP_RUN_TEST;
			MY_TCP_Client_stop(&tcp_client_buffer[i]);
			TCP_RUN_TEST;
		}
	}
}

void MY_TCP_Client_Init(struct TcpClientCon *cli , char *hostname , long port , char type , char keepalive)
{
	
	memset(cli,0x0,sizeof(struct TcpClientCon));
	cli->used = 1;
	cli->port=port;
	snprintf(cli->hostname,64,"%s",hostname);
	cli->connect_type = type;
	cli->keepalive = keepalive;
	//
}

void  MY_TCP_Client_Routing(struct TcpClientCon *cli)
{ 
	err_t err_ret;
	switch(cli->status_base)
	{
	
		case TCP_INIT:
			//开始进行DNS解析
			err_ret = dns_gethostbyname(cli->hostname,&cli->ipaddr,my_found,(void*)cli);
			DEBUGL->debug("######## err_ret %d \r\n",err_ret);
			if (err_ret == ERR_OK)
				cli->status_base = TCP_DNS_FINISH;
			else if (err_ret == ERR_INPROGRESS)
				cli->status_base = TCP_DNS_START;
			break;
		case TCP_DNS_START:
			break;
		case TCP_DNS_FINISH:
		{
			DEBUGL->debug("Start connecting ... \r\n");
			
			cli->connecting_count ++ ;	/* 每次重新连接就加1 */
			cli->pcb=tcp_new();   /*  建立通信的TCP 控制块(Clipcb) */
			
			//打开keepalive
			if (cli->keepalive > 0)
			{
				cli->pcb->so_options |= SOF_KEEPALIVE;
			}
			
			if (cli->pcb > 0)
			{
				tcp_arg(cli->pcb,(void*)cli); //设置回调函数的参数

				tcp_bind(cli->pcb,IP_ADDR_ANY,0);/*  绑定本地IP 地址和端口号*/ 
				tcp_err(cli->pcb, ClientConError); //连接失败
				
				if (tcp_connect(cli->pcb,&cli->ipaddr,cli->port,TcpCli_Connected) != 0)
				{
					DEBUGL->debug("Local tcp connect error ! \r\n");
					//TcpClient.status_base = 0
					cli->status_base = TCP_INIT;
					return ;
				}else
				{
					DEBUGL->debug("Local tcp connect success ! \r\n");
					cli->status_base = TCP_CONNECTING;
				}
			}else
			{
				cli->status_base=TCP_CONNECT_ERR;
				if (cli->connerr > 0)
					cli->connerr(cli->arg);
			}
		}
		break;
		case TCP_DNS_ERR:
		case TCP_CONNECT_ERR:
		case TCP_DISCONNECT:
			//如果是长连接，那么断线后继续连接
			if (cli->connect_type == 1)
			{
				cli->status_base = TCP_INIT;
			}
			break;
		case TCP_CONNECTING:
		case TCP_CONNECTED:
		case TCP_PAUSE:
			break;
		default:
			break;
	}
	
	if (cli->routing > 0)
		cli->routing(cli->arg);
	
} 

void MY_TCP_Client_stop(struct TcpClientCon *cli)
{

	if (cli->status_base == TCP_CONNECTED)
	{
		//tcp_close(cli->pcb);'
		tcp_abort(cli->pcb);
		
		cli->status_base = TCP_DISCONNECT;
		if (cli->disconn > 0)
			cli->disconn(cli->arg);
	}
	//
}


void MY_TCP_Client_start(struct TcpClientCon *cli)
{
	cli->status_base = TCP_INIT;
	//
}

void MY_TCP_Client_pause(struct TcpClientCon *cli)
{
	MY_TCP_Client_stop(cli);
	cli->status_base = TCP_PAUSE;
}

err_t MY_TCP_Client_write(struct TcpClientCon *cli , unsigned char *buffer , int len)
{
	return tcp_write(cli->pcb,buffer,len,TCP_WRITE_FLAG_COPY); 
	//
}



